﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public interface IEmployeeSheetService
    {
        IQueryable<EmployeeTimeSheet> GetEmployeeTimeSheets(int id);
        bool SaveNewSheet(NewSheetViewModel model);
    }
    public class EmployeeSheetService : IEmployeeSheetService
    {
        public TimesheetDb db { get; }
        public EmployeeSheetService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<EmployeeTimeSheet> GetEmployeeTimeSheets(int id)
        {
            return this.db.EmployeeTimeSheets.Where(t => t.EmployeeId == id);
        }

        public bool SaveNewSheet(NewSheetViewModel model)
        {
            if (model != null)
            {
                this.db.EmployeeTimeSheets.Add(new EmployeeTimeSheet()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = model.TaskId,
                    WorkingDate = model.WorkingDate,
                    WorkingHours = model.WorkingHours
                });
                this.db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
